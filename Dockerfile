FROM alpine:latest
MAINTAINER Chance Hudson

RUN apk add --update nodejs-npm python make git gcc g++

COPY . /src
WORKDIR /src

RUN npm ci

RUN npm run build-server

CMD ["npm", "run", "server"]

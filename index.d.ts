declare module 'unified-stream'
declare module 'remark-rehype'
declare module 'rehype-stringify'
declare module 'remark-slug'
declare module 'rehype-sanitize'
declare module 'rehype-highlight'
declare module 'rehype-raw'
declare module 'remark-retext'
declare module 'retext-simplify'
declare module 'retext-english'
declare module 'retext-sentiment'
declare module 'retext-indefinite-article'
declare module 'retext-passive'
declare module 'retext-repeated-words'
declare module 'retext-stringify'

declare module 'diff-match-patch'

interface Window {
  __INITIAL_STATE__: any
}

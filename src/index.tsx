import Vue from 'vue'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import App from './App'
import Home from './Home'
import EditNote from './EditNote'
import CreateNote from './CreateNote'
import Note from './Note'
import UserPage from './UserPage'
import AccountPage from './AccountPage'
import NoteHistory from './NoteHistory'
import NoteRevision from './NoteRevision'
import Drafts from './Drafts'
// import EditUser from './EditUser'
import NoteStore from './stores/notes'
import UserStore from './stores/users'
import ColorStore from './stores/colors'
import ReadingStore from './stores/reading'
import MarkdownStore from './stores/markdown'
import storage from './logic/storage'
import Meta from 'vue-meta'

export function createApp(cookie?: string) {
  Vue.use(VueRouter)
  Vue.use(Vuex)
  Vue.use(Meta)
  storage._cookie = cookie
  const store = new Vuex.Store({
    state: {
      focused: false,
      url: '',
      history: [],
      nextPath: '',
    },
    mutations: {
      setUrl: (state: any, url: string) => {
        state.url = url
      },
      blur: (state: any) => {
        state.focused = false
      },
      focus: (state: any) => {
        state.focused = true
      },
      addHistory: (state: any, path: string) => {
        state.history = [
          ...state.history,
          path,
        ]
      },
      popHistory: (state: any) => {
        if (state.history.length === 0) {
          state.nextPath = '/'
          return
        }
        state.nextPath = state.history.pop()
      }
    },
    actions: {},
    modules: {
      notes: NoteStore(),
      users: UserStore(),
      colors: ColorStore(),
      reading: ReadingStore(),
      markdown: MarkdownStore(),
    },
  })
  const router = new VueRouter({
    mode: 'history',
    scrollBehavior: async (to: any, from: any, savedPosition: any) => {
      if (to.hash) {
        // await new Promise(r => Vue.nextTick(r))
        return {
          selector: to.hash,
          offset: {
            y: 100
          }
        }
      }
      if (savedPosition) {
        await new Promise(r => setTimeout(r, 100))
        return savedPosition
      } else {
        return { x: 0, y: 0 }
      }
    },
    routes: [
      { path: '/', component: Home },
      { path: '/edit/:index', component: EditNote },
      { path: '/new', component: CreateNote },
      { path: '/drafts', component: Drafts },
      { path: '/notes/:index/history', component: NoteHistory, },
      { path: '/notes/:index/revision/:revindex', component: NoteRevision, },
      { path: '/notes/:index', component: Note },
      { path: '/account', component: AccountPage, },
      { path: '/:username', component: UserPage, },
    ]
  })
  const app = new Vue({
    router,
    store,
    render: h => h(App),
  })
  const auth = storage.getCookie('auth')
  if (auth) {
    store.commit('setActiveUser', auth)
  }
  router.beforeEach((to, from, next) => {
    if (to.path === store.state.nextPath) {
      store.state.nextPath = ''
    } else {
      store.commit('addHistory', from.path)
    }
    next()
  })
  return { app, router, store }
}

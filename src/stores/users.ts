import axios from 'axios'
import storage from '../logic/storage'

export interface User {
  _id: string
  username: string
  createdAt: string|Date
  updatedAt: string|Date
  passwordHash: string // TODO: If this is received throw a warning
  token?: string
}

export default () => ({
  state: {
    token: '',
    account: {},
    transactions: [],
    subscription: {},
    subscriptionOffer: {},
    activeUser: {},
    usersById: {},
    usersByUsername: {},
    preferences: {
      fontFamily: 'helvetica',
    },
    follows: {
      following: [],
      followers: [],
    },
    editingUser: {},
    editingUserBackup: {},
  },
  mutations: {
    setFont: (state: any, payload: string) => {
      state.preferences.fontFamily = payload
    },
    addUser: (state: any, payload: User) => {
      state.usersById = {
        ...state.usersById,
        [payload._id]: { ...payload, token: '' },
      }
      state.usersByUsername = {
        ...state.usersByUsername,
        [payload.username]: { ...payload, token: '' },
      }
    },
    setActiveUser: (state: any, payload: User) => {
      state.activeUser = payload
      state.token = payload.token || state.token
      storage.setCookie('auth', {
        ...payload,
        token: state.token,
      })
      state.usersById[payload._id] = {
        ...payload,
        token: '',
      }
    },
    beginEditingUser: (state: any, payload: User) => {
      state.editingUserBackup = payload
      state.editingUser = payload
    },
    editUser: (state: any, payload: User) => {
      state.editingUser = {
        ...state.editingUser,
        ...payload,
      }
    },
  },
  actions: {
    load: async ({ dispatch, commit, state }: any) => {
      if (!state.token) return
      await dispatch('loadActiveUser')
      await dispatch('loadAccount')
      if (state.activeUser.activeSubscription) {
        await dispatch('loadSubscription')
      }
      if (state.activeUser.needsRefresh) {
        await dispatch('refreshToken')
      }
    },
    loadActiveUser: async ({ dispatch, commit, state}: any) => {
      const { data } = await axios.post('/get/users/authed', {
        token: state.token,
      })
      commit('setActiveUser', data)
      commit('addUser', data)
    },
    loadTransactions: async ({ state }: any) => {
      const { data } = await axios.post('/get/transactions', {
        token: state.token,
      })
      const { transactions, account } = data
      state.account = account
      state.transactions = transactions
    },
    loadSubscriptionPrice: async ({ state }: any) => {
      const { data: subscriptionOffer } = await axios.post('/get/subscription/price', {
        token: state.token,
      })
      state.subscriptionOffer = subscriptionOffer
    },
    loadSubscription: async ({ state }: any) => {
      try {
        const { data: subscription } = await axios.post('/get/subscription', {
          token: state.token,
        })
        state.subscription = subscription
      } catch (err) {
        console.log('Error loading subscription', err)
      }
    },
    subscribe: async ({ dispatch, state }: any) => {
      const { data: subscription } = await axios.post('/put/subscription', {
        token: state.token,
      })
      state.subscription = subscription
    },
    loadAccount: async ({ dispatch, state }: any, payload: number = 0) => {
      try {
        if (payload > 6) return
        const { data: account } = await axios.post('/get/account', {
          token: state.token,
        })
        state.account = account
      } catch (err) {
        await new Promise(r => setTimeout(r, 10000))
        await dispatch('loadAccount', payload + 1)
      }
    },
    loadFollowers: async ({ state }: any) => {
      const { data } = await axios.post('/get/follows', {
        token: state.token,
      })
      state.follows = data
    },
    follow: async ({ state }: any, payload: {
      _id: string,
    }) => {
      await axios.post(`/put/follow/${payload._id}`, {
        token: state.token
      })
    },
    login: async ({ dispatch, commit, state }: any, payload: {
      username: string,
      password: string,
    }) => {
      const { data } = await axios.post('/users/login', {
        ...payload,
        userAgent: navigator.userAgent,
        platform: navigator.platform,
        cookieEnabled: navigator.cookieEnabled,
        language: navigator.language,
      })
      commit('setActiveUser', data)
      commit('addUser', data)
      // Asynchronously
      dispatch('loadAccount')
    },
    createUser: async ({ dispatch, commit, state }: any, payload: {
      username: string,
      password: string,
    }) => {
      const { data } = await axios.post('/users', {
        ...payload,
        userAgent: navigator.userAgent,
        platform: navigator.platform,
        cookieEnabled: navigator.cookieEnabled,
        language: navigator.language,
      })
      commit('setActiveUser', data)
      commit('addUser', data)
      // Asynchronously
      setTimeout(() => {
        dispatch('loadAccount')
      }, 30000)
    },
    refreshToken: async ({ commit, state }: any) => {
      const { data } = await axios.post('/put/users/token/refresh', {
        token: state.token,
        // Session metadata
        userAgent: navigator.userAgent,
        platform: navigator.platform,
        cookieEnabled: navigator.cookieEnabled,
        language: navigator.language,
      })
      commit('setActiveUser', data)
      commit('addUser', data)
    },
    loadUser: async ({ commit, state }: any, payload: any) => {
      const { data: user } = await axios.post(`/get/users/${payload._id || payload.username}`)
      commit('addUser', user)
      return user
    },
    updateUser: async ({ commit, state }: any, payload: any) => {
      const { data: user } = await axios.post('/put/users/edit', {
        ...payload,
        token: state.token,
      })
      commit('addUser', user)
    },
    updateAccount: async ({ dispatch, state }: any, payload: {
      autorenew?: boolean,
      payoutAddress?: string,
    }) => {
      await axios.post(`/put/accounts/${state.account._id}`, {
        ...payload,
        token: state.token,
      })
      await dispatch('loadAccount')
    }
  }
})

import axios from 'axios'
import { User } from './users'
import storage from '../logic/storage'
import diff_match_patch from 'diff-match-patch'
import { markdownToHtml, markdownToHtmlSync } from './markdown'
import uniqby from 'lodash.uniqby'

export interface Note {
  _id: string
  ownerId: string
  text: string
  description: string
  renderedText?: string
  renderedDescription?: string
  index: number|string // unique, incrementing from 0
  createdAt: string|Date
  updatedAt: string|Date
  owner?: User
  reactions: { reaction: string, count: number }[]
}

export interface Patch {
  _id: string
  noteId: string
}

export default () => ({
  state: {
    notes: [] as Note[],
    notesById: {},
    notesByIndex: {},
    notesByUsername: {},
    editingNote: {
      text: 'Loading...'
    },
    patchesByNoteIndex: {},
    draftNote: {},
    drafts: [],
    revisionsByNoteIndex: {},
  },
  mutations: {
    editNote: (state: any, note: Note) => {
      const update = {
        ...state.editingNote,
        ...note,
        updatedAt: new Date(),
      }
      state.editingNote = {
        ...update,
        renderedDescription: markdownToHtmlSync(update.description),
        renderedText: markdownToHtmlSync(update.text),
      }
    },
    resetEditNote: (state: any) => {
      state.editingNote = {
        text: 'Loading...'
      }
    },
    draftNote: (state: any, note: Note) => {
      const update = {
        ...state.draftNote,
        ...note,
        updatedAt: new Date(),
      }
      state.draftNote = {
        ...update,
        renderedDescription: markdownToHtmlSync(update.description),
        renderedText: markdownToHtmlSync(update.text),
      }
      storage.setCookie('draftNote', state.draftNote)
    },
    addNote: (state: any, _note: Note) => {
      const note = renderNoteSync(_note)
      state.notesById = {
        ...state.notesById,
        [note._id]: note,
      }
      state.notesByIndex = {
        ...state.notesByIndex,
        [note.index]: note,
      }
    },
    addNotesForUser: (state: any, payload: {
      username: string,
      notes: Note[],
    }) => {
      const { username, notes } = payload
      state.notesByUsername = {
        ...state.notesByUsername,
        [username]: notes.map(renderNoteSync)
      }
    },
    addPatches: (state: any, payload: {
      noteIndex: number,
      patches: Patch[],
    }) => {
      const { noteIndex, patches } = payload
      state.patchesByNoteIndex = {
        ...state.patchesByNoteIndex,
        [noteIndex]: patches,
      }
    },
  },
  actions: {
    load: async ({ dispatch }: any) => {
      await dispatch('loadNotes')
    },
    loadDraftNote: ({ commit }: any) => {
      try {
        const note = storage.getCookie('draftNote')
        commit('draftNote', note)
      } catch (err) {
        console.log('Failed to load saved draft')
      }
    },
    loadDrafts: async ({ rootState, commit, state }: any) => {
      const { data: drafts } = await axios.post('/get/notes/drafts', {
        token: rootState.users.token,
      })
      for (const note of drafts) {
        commit('addNote', note)
        if (note.owner) {
          commit('addUser', note.owner)
        }
      }
      state.drafts = drafts.map(renderNoteSync)
    },
    loadRelatedNotes: async ( { commit, rootState }: any, payload: {
      noteId: string
    }) => {
      const { data: notes } = await axios.post(`/notes/${payload.noteId}/related`, {
        token: rootState.users.token,
      })
      for (const note of notes) {
        commit('addNote', note)
        if (notes.owner) {
          commit('addUser', note.owner)
        }
      }
      return notes.map(renderNoteSync)
    },
    loadFeed: async ({ commit, state }: any, payload: {
      usernames: string[],
    }) => {
      const { data: notes } = await axios.post('/get/notes', {
        usernames: JSON.stringify(payload.usernames),
      })
      for (const note of notes) {
        commit('addNote', note)
        if (note.owner) {
          commit('addUser', note.owner)
        }
      }
      if (payload.usernames) {
        commit('addNotesForUser', {
          username: payload.usernames[0],
          notes,
        })
      }
      return notes.map(renderNoteSync)
    },
    loadNotes: async ({ commit, state }: any) => {
      const { data: notes } = await axios.post('/get/notes')
      for (const note of notes) {
        commit('addNote', note)
        if (note.owner) {
          commit('addUser', note.owner)
        }
      }
      const _notes = uniqby([...state.notes, ...notes], '_id')
      state.notes = _notes.map(renderNoteSync)
    },
    createNote: async ({ state, dispatch, rootState, commit }: any, payload: {
      text: string,
    }) => {
      const { text, description, isDraft, isUnmonetized } = state.draftNote
      const { data } = await axios.post('/notes', {
        text,
        description,
        isDraft,
        isUnmonetized,
        token: rootState.users.token,
      })
      commit('addNote', data)

      // Reset draft and cookies
      state.draftNote = {}
      commit('draftNote', {})

      // Reload the notes after creating
      await dispatch('loadNotes')
      return renderNoteSync(data)
    },
    updateNote: async ({ commit, state, rootState }: any) => {
      const { _id, text, description, isDraft } = state.editingNote
      const { data: note } = await axios.post(`/put/notes/${_id}`, {
        text,
        description,
        isDraft,
        token: rootState.users.token
      })
      commit('addNote', note)
    },
    loadNote: async ({ commit, state, rootState }: any, indexOrId: string) => {
      const { data: note } = await axios.post(`/get/notes/${indexOrId}`, {
        token: rootState.users.token,
      })
      commit('addNote', note)
      if (note.owner) {
        commit('addUser', note.owner)
      }
    },
    deleteNote: async ({ state, rootState }: any, payload: Note) => {
      await axios.post(`/delete/notes/${payload._id}`, {
        token: rootState.users.token,
      })
      delete state.notesById[payload._id]
      state.notes = state.notes.filter((note: Note) => note._id !== payload._id)
    },
    loadNoteHistory: async ({ commit, state, rootState }: any, payload: {
      index: number
    }) => {
      const { data: patches } = await axios.post(`/get/notes/${payload.index}/revisions`, {
        token: rootState.users.token,
      })
      for (const patch of patches) {
        const dmp = new diff_match_patch()
        const textPatches = dmp.patch_fromText(patch.textPatch)
        const descriptionPatches = dmp.patch_fromText(patch.descriptionPatch)
        patch.textHtml = patchesToHtml(textPatches)
        patch.descriptionHtml = patchesToHtml(descriptionPatches)
        const {
          additions: textAdditions,
          deletions: textDeletions
        } = patchesToCounts(textPatches)
        const {
          additions: previewAdditions,
          deletions: previewDeletions
        } = patchesToCounts(descriptionPatches)
        patch.textAdditions = textAdditions
        patch.textDeletions = textDeletions
        patch.previewAdditions = previewAdditions
        patch.previewDeletions = previewDeletions
      }
      commit('addPatches', {
        noteIndex: payload.index,
        patches,
      })
    },
    loadNoteRevision: async ({ commit, state, rootState }: any, payload: {
      noteIndex: number,
      revisionIndex: number,
    }) => {
      const { noteIndex, revisionIndex } = payload
      const { data: note } = await axios.post(`/get/notes/${noteIndex}/revision/${revisionIndex}`)
      await renderNote(note)
      if (note.owner) {
        commit('addUser', note.owner)
      }
      state.revisionsByNoteIndex = {
        ...state.revisionsByNoteIndex,
        [noteIndex]: {
          ...(state.revisionsByNoteId || {}),
          [revisionIndex]: note,
        },
      }
    }
  }
})

export function renderNoteSync(_note: Note) {
  const note = {
    ..._note,
  }
  if (note.description) {
    note.renderedDescription = markdownToHtmlSync(note.description)
  } else {
    note.renderedDescription = ''
  }
  if (note.text) {
    note.renderedText = markdownToHtmlSync(note.text)
  } else {
    note.renderedText = ''
  }
  return note
}
/**
 * Render html for note description and text
 **/
export async function renderNote(note: Note) {
  let p1 = Promise.resolve(note.renderedDescription)
  let p2 = Promise.resolve(note.renderedText)
  if (!note.renderedDescription) {
    p1 = markdownToHtml(note.description)
  }
  if (!note.renderedText) {
    p2 = markdownToHtml(note.text)
  }
  const [ renderedDescription, renderedText ] = await Promise.all([
    p1,
    p2
  ])
  note.renderedDescription = renderedDescription
  note.renderedText = renderedText
  return note
}

/**
 * Calculate the number of characters inserted and deleted
 **/
function patchesToCounts(patches: any) {
  const count = {
    additions: 0,
    deletions: 0,
  }
  for (const patch of patches) {
    for (let x = 0; x < patch.diffs.length; x++) {
      const op = patch.diffs[x][0]    // Operation (insert, delete, equal)
      const data = patch.diffs[x][1]  // Text of change.
      switch (op) {
        case diff_match_patch.DIFF_INSERT:
          count.additions += data.length
          break
        case diff_match_patch.DIFF_DELETE:
          count.deletions += data.length
          break
      }
    }
  }
  return count
}

/**
 * Generate HTML representation of diffs
 **/
function patchesToHtml(patches: any) {
  const htmls = []
  const pattern_amp = /&/g
  const pattern_lt = /</g
  const pattern_gt = />/g
  const pattern_para = /\n/g
  for (const patch of patches) {
    const html = []
    for (let x = 0; x < patch.diffs.length; x++) {
      const op = patch.diffs[x][0]    // Operation (insert, delete, equal)
      const data = patch.diffs[x][1]  // Text of change.
      const text = data
        .replace(pattern_amp, '&amp;')
        .replace(pattern_lt, '&lt;')
        .replace(pattern_gt, '&gt;')
        .replace(pattern_para, '<br>')
      switch (op) {
        case diff_match_patch.DIFF_INSERT:
          html[x] = '<ins>' + text + '</ins>'
          break
        case diff_match_patch.DIFF_DELETE:
          html[x] = '<del>' + text + '</del>'
          break
        case diff_match_patch.DIFF_EQUAL:
          html[x] = '<span>' + text + '</span>'
          break
      }
    }
    htmls.push(html.join(''))
  }
  return htmls.join('<br />')
}

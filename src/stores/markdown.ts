import unified from 'unified'
import stringify from 'retext-stringify'
import markdown from 'remark-parse'
import remark2rehype from 'remark-rehype'
import html from 'rehype-stringify'
import toc from 'remark-toc'
import slug from 'remark-slug'
// import sanitize from 'rehype-sanitize'
import highlight from 'rehype-highlight'
import raw from 'rehype-raw'
import 'highlight.js/styles/atom-one-dark.css'
import visit from 'unist-util-visit'
import remark2retext from 'remark-retext'
// import simplify from 'retext-simplify'
import english from 'retext-english'
// import sentiment from 'retext-sentiment'
// import indefinite from 'retext-indefinite-article'
// import passive from 'retext-passive'
// import repeat from 'retext-repeated-words'

export default () => ({
  state: {

  },
  mutations: {

  },
  actions: {
    markdownToHtml: async ({}: any, text: string) => {
      return await markdownToHtml(text)
    }
  },
})

function markdownProcessor() {
  const processor = unified()
    .use(markdown)
    .use(slug)
    .use(toc)
    .use(noteReference as any)
    .use(remark2rehype, {
      allowDangerousHTML: true,
    })
    .use(highlight, {
      ignoreMissing: true,
    })
    .use(raw)
    // .use(sanitize)
    .use(html)
  return processor
}

export async function markdownToHtml(text: string) {
  const processor = markdownProcessor()
  const { contents } = await processor.process(text)
  return contents.toString()
}

export function markdownToHtmlSync(text: string) {
  const processor = markdownProcessor()
  const { contents } = processor.processSync(text)
  return contents.toString()
}

export async function markdownToText(text: string) {
  const processor = unified()
    .use(markdown)
    .use(
      remark2retext,
      unified().use(english)
    )
    .use(stringify)
  const { contents } = await processor.process(text)
  return contents.toString()
}

function noteReference(options: unified.ProcessorSettings<unified.Settings>) {
  const exclude = ['code', 'link']
  function visitor(node: any, index: number, parent: any) {
    if (!parent) return
    if (exclude.indexOf(node.type) !== -1) return
    if (exclude.indexOf(parent.type) !== -1) return
    if (!node.value) return
    const noteRegex = /\s#[0-9]+(?![a-zA-Z])/g
    const result = noteRegex.exec(node.value)
    if (result === null) return
    const [ match ] = result
    const num = match.slice(2)
    const [ prefix, ...suffixes ] = node.value.split(match)
    const suffix = suffixes.join(match)
    node.value = prefix
    // let baseUrl = process.env.URL || 'https://dev.epochrumor.com'
    // if (global.window && window.location.href.indexOf('https://epochrumor.com') !== -1) {
    //   baseUrl = 'https://epochrumor.com'
    // }
    parent.children.splice(index+1, 0,
      {
        type: 'link',
        url: `https://epochrumor.com/notes/${num}`,
        children: [{
          type: 'text',
          value: match,
        }]
      },
      {
        type: 'text',
        value: suffix,
      }
    )
  }
  return function transformer(tree: any) {
    visit(tree, visitor)
  }
}

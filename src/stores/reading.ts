import axios from 'axios'
import storage from '../logic/storage'

const SESSION_VIEWS_COOKIE = 'sessionViews'

export default () => ({
  state: {
    activeNoteId: '',
    sessionViews: [],
    cachedViews: [],
  },
  mutations: {
    resetSessions: (state: any) => {
      state.sessionViews = []
      storage.setCookie(SESSION_VIEWS_COOKIE, state.sessionViews)
    },
    logView: (state: any, payload: any) => {
      state.sessionViews = [
        ...state.sessionViews,
        payload,
      ]
      storage.setCookie(SESSION_VIEWS_COOKIE, state.sessionViews)
    },
  },
  actions: {
    load: async ({ state }: any) => {
      const views = storage.getCookie(SESSION_VIEWS_COOKIE) || []
      state.sessionViews = [
        ...state.sessionViews,
        ...(views),
      ]
    },
    uploadViews: async ({ rootState, state }: any) => {
      if (!rootState.users.token) return
      const views = [...state.sessionViews]
      state.sessionViews = []
      try {
        const { data } = await axios.post(`/noteviews`, {
          views,
          token: rootState.users.token,
        })
        if (data.createdCount !== views.length) {
          console.log('View creation count mismatch', views.length, data.createdCount)
        }
        storage.setCookie(SESSION_VIEWS_COOKIE, state.sessionViews)
      } catch (err) {
        state.sessionViews = [
          ...state.sessionViews,
          ...views,
        ]
        storage.setCookie(SESSION_VIEWS_COOKIE, state.sessionViews)
        console.log('Failed to upload session views')
      }
    },
  }
})

export const colors = {
  gray: 'rgba(40, 44, 51, 1)',
  darkGray: 'rgba(33, 38, 43, 1)',
  purple: 'rgba(198, 120, 221, 1)',
  lightWhite: 'rgba(210, 215, 224, 1)',
  white: 'rgba(181, 188, 201, 1)',
  darkWhite: 'rgba(75, 83, 98, 1)',
  blue: 'rgba(95, 170, 233, 1)',
  green: 'rgba(152, 195, 121, 1)',
  darkRed: 'rgba(164, 48, 57, 1)',
  red: 'rgba(184, 58, 67, 1)',
  amber: 'rgba(219, 154, 102, 1)',
}


export default () => ({
  state: {
    ...colors,
    background: colors.darkGray,
    headerBackground: colors.gray,
    headerColor: colors.purple,
    color: colors.white,
    noteBackground: colors.gray,
    noteBorder: `1px solid ${colors.darkWhite}`,
  }
})

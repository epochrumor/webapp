import axios from 'axios'
import { createApp } from '.'

axios.defaults.baseURL = process.env.API_URL || 'http://localhost:4000'

export default (context: any) => {
  return new Promise(async (rs, rj) => {
    const { app, router, store } = createApp(context.cookie)
    store.commit('setUrl', process.env.URL || 'http://localhost:8080')
    const { url } = context
    const { fullPath } = router.resolve(url).route

    if (fullPath !== url) {
      return rj({ url: fullPath })
    }
    router.push(url)
    router.onReady(async () => {
      try {
        const matchedComponents = router.getMatchedComponents()
        if (!matchedComponents.length) {
          return rj({ code: 404 })
        }
        context.state = store.state
        context.meta = app.$meta()
        rs(app)
      } catch (err) {
        rj(err)
      }
    }, rj)
  })
}

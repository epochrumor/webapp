
// Remote static asset storage

function storageUrl() {
  if (window.location.href.indexOf('dev.epochrumor.com') !== -1) {
    return 'https://dev.static.epochrumor.com'
  } else if (window.location.href.indexOf('epochrumor.com') !== -1) {
    return 'https://static.epochrumor.com'
  } else {
    return 'https://dev.static.epochrumor.com'
  }
}

function isNode() {
  if (typeof process === 'object') {
    if (typeof process.versions === 'object') {
      if (typeof process.versions.node !== 'undefined') {
        return true
      }
    }
  }
  return false
}

class Storage {
  _cookie?: string

  constructor(cookie?: string) {
    this._cookie = cookie
  }

  get cookie() {
    if (this._cookie) {
      return this._cookie
    }
    if (!isNode() && document) {
      return document.cookie
    }
    return ''
  }

  async upload(file: any, token: string) {
    if (!file) return
    if (file.size > 1024 * 1024 * 5) return
    const formData = new FormData()
    formData.append('filedata', file)
    const data = await fetch(`${storageUrl()}/upload?token=${token}`, {
      method: 'POST',
      body: formData,
    })
    const { url } = await data.json()
    return url
  }

  // Local cookie storage

  setCookie(name: string, value: Object) {
    const serialized = encodeURIComponent(JSON.stringify(value))
    if (!isNode() && document) {
      document.cookie = `${name}=${serialized};path=/;`
    }
  }

  lastCookieString: string
  cookiesByName: { [key: string]: string } = {}
  cachedCookie(name: string): string|void {
    if (this.lastCookieString === this.cookie) return this.cookiesByName[name]
    const cookies = this.cookie.split(';')
    this.cookiesByName = {}
    for (const cookie of cookies) {
      const [ name, value ] = cookie.split('=')
      this.cookiesByName[(name || '').trim()] = (value || '').trim()
    }
    this.lastCookieString = this.cookie
    return this.cookiesByName[name]
  }

  getCookie(name: string) {
    const cookie = this.cachedCookie(name)
    if (!cookie) return
    // All cookies should be json encoded
    try {
      return JSON.parse(decodeURIComponent(cookie))
    } catch (err) {
      console.log(`Error decoding ${name} cookie`)
      console.log(err)
      return
    }
  }
}

export default new Storage()

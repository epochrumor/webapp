import nanoid from 'nanoid'
import { colors } from '../stores/colors'

export const unixtime = () => `${Math.floor(+(new Date()) / 1000)}`

const colorRotation = [
  colors.purple,
  colors.green,
  colors.blue,
]

export const currentColor = () => {
  const index = +unixtime() % colorRotation.length
  return colorRotation[index]
}

const listeners = {} as { [key: string]: Function }

export function everySecond(func: Function) {
  const id = nanoid()
  listeners[id] = func
  return id
}

export function clearEverySecond(id: string) {
  delete listeners[id]
}

function notifyListeners() {
  for (const func in listeners) {
    listeners[func]()
  }
}

(async () => {
  // Wait until the next half second offset
  const waitTime = (+(new Date()) % 1000) + 500
  // Wait until a half second offset
  await new Promise(r => setTimeout(r, waitTime/2))
  notifyListeners()
  // Split the wait time in 2 to prevent a 2 second jump
  await new Promise(r => setTimeout(r, waitTime/2))
  notifyListeners()
  setInterval(notifyListeners, 1000)
})()

import Vue from 'vue'
import axios from 'axios'
import { createApp } from '.'

const { app, router, store } = createApp()

document.title = 'Epoch Rumor'
document.body.style.backgroundColor = (store.state as any).colors.darkGray

axios.defaults.baseURL = 'http://localhost:4000'

if (window.location.href.indexOf('dev.epochrumor.com') !== -1) {
  axios.defaults.baseURL = 'https://dev.api.epochrumor.com'
  store.commit('setUrl', 'https://dev.epochrumor.com')
} else if (window.location.href.indexOf('/epochrumor.com') !== -1) {
  axios.defaults.baseURL = 'https://api.epochrumor.com'
  store.commit('setUrl', 'https://epochrumor.com')
}
store.commit('setUrl', `${window.location.protocol}//${window.location.host}`)

if (window.__INITIAL_STATE__) {
  store.replaceState(window.__INITIAL_STATE__)
}

store.state.focused = !document.hidden
// Reset on new page enter/reload
store.state.history = []

window.addEventListener('visibilitychange', () => {
  store.state.focused = !document.hidden
})

store.dispatch('load')

router.onReady(() => {
  app.$mount('#app')
})
